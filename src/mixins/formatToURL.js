// Function actually defined in grimFunctions.js


import { titleToURL, playlistToURL } from '../grimmFunctions';


export const formatToURL = {
  methods: {
    titleToURL,
    playlistToURL,
  },
  // computed: {
  //   toCustomPlaylist() {
  //     let customURL = this.playlistToURL(this.$store.state.customPlaylist);
  //     return `/music/custom/${customURL}`;
  //   },
  // },
};

