// About loadWork(work){}
// If loading a track from the Full Playlist (playlistState = browse), we navigate to the track to give
// each work a dynamic URL. The Collection and custom Playlists use this function load tracks from
// the state as their dynamic URLs are reserved for saving the Collection and Playlist.
// Used in Playlist and Player components...as of this writing
import { titleToURL } from '../grimmFunctions';

// Load work will need to be refactor when going to dynamoDbd
// ADD AutoPlay Option in the parameters

const loadWork = {
  methods: {
    loadWork(work, autoPlay) {
      if (this.$store.state.route.params.playlistStateURL === 'browse') { // this will be track
        this.$store.commit('setFeaturedWork', work);
        let trackURL = titleToURL(work.title);
        this.$router.push(`/music/browse/${trackURL}`);
      } else {
        // this only actually updates the state because the featuredPlayer has an actual mutation on the updated() hook
        // below is bullshit
        this.$store.commit('setFeaturedWork', work);
      }
      if (autoPlay) {
        this.$store.commit('setAutoPlay', true);
      }
    },
  },
};


export { loadWork };
