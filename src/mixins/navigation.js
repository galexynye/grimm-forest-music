import { titleToURL, playlistToURL } from '../grimmFunctions';

export const navigation = {
  methods: {
    navigateHome() {
      this.$router.push('/');
    },
    navigateToAbout() {
      this.$router.push('/#about');
    },
    navigateToBrowse() {
      this.$router.push('/music/browse');
    },
    navigateToCustomPlaylist(customPlaylist) {
      let customURL = playlistToURL(customPlaylist);
      this.$router.push(`/music/custom/${customURL}`);
    },
    navigateToCollection(workCollection) {
      this.$router.push(`/music/collection/${titleToURL(workCollection)}`);
    },
    navigateToPortfolio() {
      this.$router.push('/portfolio');
    },
    navigateToPortfolioWork(workTitle) {
      this.$router.push(`/portfolio/work/${titleToURL(workTitle)}`);
    },
    navigateToContact() {
      this.$router.push('/contact');
    },
  },

};
