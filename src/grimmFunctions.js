
// FORMATING URLs --- titleToURL will be changed to trackIDToURL
export function titleToURL(workTitle) {
  if (workTitle) {
    // return workTitle.replace(/\W/g, '-').toLowerCase();
    return workTitle.replace(/[^A-Za-z0-9]/g, '-').toLowerCase(); // Get's rid of underscores too
  }
  return false;
}

export function playlistToURL(playlist) {
  let urlFriendlyCustomPlaylist = [];
  playlist.forEach((work) => {
    urlFriendlyCustomPlaylist.push(titleToURL(work.title));
  });
  let customPlaylistURL = urlFriendlyCustomPlaylist.join('_');
  return customPlaylistURL;
}


/* Add the setStateViaURL functions here from  mustatations in store.js.
This way the function is in one place.
Can Only have 2 params.
second param is an object. Because in mutations the
first param is always state, when it is called in the
main.js, we'll call the function like so setStateViaURL(store.state, {})
In mutations it will be called setStateViaURL(state, {}).
We set state in mutations for initial load of a page with a playlist, track or collection.
We set state in with the router in main.js for all other site navigations.
*/
