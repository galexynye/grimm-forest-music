import Vue from 'vue';
import Router from 'vue-router';
// import { store } from '../store/store';

import Home from '../components/Home';
import FeaturedPlayer from '../components/FeaturedPlayer';
import Portfolio from '../components/Portfolio';
import Contact from '../components/Contact';

Vue.use(Router);

// The prop names for different routes need to be different (in components)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home,
      name: 'home',
    },
    {
      path: '/music',
      component: FeaturedPlayer,
    },
    {
      path: '/music/:playlistStateURL',
      component: FeaturedPlayer,
      props: true,
    },
    {
      path: '/music/:playlistStateURL/:tracksURL',
      component: FeaturedPlayer,
      props: true,
    },
    {
      path: '/portfolio',
      component: Portfolio,
    },
    {
      path: '/portfolio/work/:portfolioWork',
      component: Portfolio,
    },
    {
      path: '/contact',
      component: Contact,
    },
    {
      path: '*', redirect: '/',
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        // , offset: { x: 0, y: 10 }
      };
    } else if (savedPosition) {
      return savedPosition;
      // return { x: 0, y: 0 };
    }
    return { x: 0, y: 0 };
  },

});

