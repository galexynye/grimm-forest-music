// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlay, faPause, faStepBackward, faStepForward, faVolumeOff, faStop, faDownload, faCopy, faPlus, faMinus, faCompactDisc, faShoppingCart, faAngleLeft, faAngleRight, faBars, faTimes, faTimesCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import App from './App';
import router from './router';
import { store } from './store/store';


library.add(faPlay, faPause, faStepBackward, faStepForward, faVolumeOff, faStop, faDownload,
  faCopy, faPlus, faMinus, faCompactDisc, faShoppingCart, faAngleLeft, faAngleRight, faBars, faTimes,
  faTimesCircle, faInfoCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);


sync(store, router); // Puts the router into the state

// Import is not working on my NPMs for somereason. Use the required metheod below
const VueMasonryPlugin = require('vue-masonry').VueMasonryPlugin;

Vue.use(VueMasonryPlugin);

// Note sure why this is showing an error because vue-clipboard works. Seems to be the order of the const delcarations/require() will throw an error depending on the order.
const VueClipboard = require('vue-clipboard2');

Vue.use(VueClipboard);

const VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 2000,
  easing: 'ease',
  offset: -75,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true,
});


Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
});

