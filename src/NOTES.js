/*

-To Do
    -Contact Section
    -TAGs / Genres
        -Generate Tags in data from function. So tags are coming
        from the playlist, Every tag that a piece of music has is
        Generated


============================= General Notes to Help =================================
    -BEWARE MIXINS - functions are that are called on components that seem to come from nowhere are probably
    imported in from a mixin. Just in case you forget.

============================= Hard Features to Add =================================
    - Drag and drop custom playlist re-order

============================= Open Bugs ======================================
        -BUG BUG BUG BUG - if a track title contains an underscore it will not load on when rendering a 'customPlaylist'
            or collection via URL ---this won't be fixed with track id...need to add removing underscores in grimmfunctions

============================= CDN Installs ======================================
    - Google Fonts - CDN
    - Wavesurfer  - to render waveforms of mp3s
        CDN - <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        https://jsfiddle.net/07wtoL9g/1/ Example of Vue
        https://wavesurfer-js.org/docs/options.html
    -Font Awesome CDN -- needs to be uploaded


=========================== Library Installs ==================================
- vuex-router-sync ----puts the routes into state. USED FOR THE PLAYLISTSTATE extensively and rendering playlists


=========================== SPAGHETTI CODE NOTES ==================================
-Duplicate functions
     -loadWork() is repeated in Playlist and Player Components.
     In the playlist component for loading a track,
     and in the Player component for the navigation buttons

- URL Conversion - (Luckily, it's all contained in the grimm functions for convertions)
    - The URL CONVERSION FUNCTION IS LOCATED IN grimmFunctions.js
        - everything is lowercase
            - all non - letter, numbers and underscores are removed and replaced with hyphens...ie Breaking In! becomes breaking -in -
                -all tracks are separated by underscores breaking -in -_fuzzy - chaos_ - death - is - near -
                    -BUG BUG BUG BUG - if a track title contains an underscore it will not load on when rendering a 'customPlaylist'
or collection via URL

    - TRACK TITLES CANNOT CONTAIN UNDERSCORES Right now
        - There can be no duplicate track Titles!!!!
            - With this playlist / dynamic changing track titles means that if a title is changed
then anyone who custom a playlist with that title will not load the track again
    - Should refactor to give each track a Grimm Number but I CAN'T
        - BE CAREFUL: RENDERING OF DYNAMIC URL DEPENDS ON TITLE
            - In the Data add a "displayTitle" - Property with the Name ? for rendering dom
                - Change title to ISRC codes ?
                    -could refactor to use IIRSC codes or whatever the record code are and a title
                        - Titles with spaces and hyphones ie The Spy or The - Spy for all intents and
    purposes are the same and can not be used

=========================== How the FeaturedPlayer component works ie (Browse / custom Playlist / Collections Work) ===========================
-URL sets a playlist state as well as gives a track or playlist to load
-The Playlist component renders a playlist based on the playlist state

-THe first time a URL is loaded, it gets loaded into the state
via the created() hook
-After that in the main.js file there is a global router gaurd
called before each that checks if the param is coming in, if it
is, then it sets the playlist state (the playlist will then update on
the state change)

=========================    Vue Notes ==============================
$ signals that it comes from Vue or Package, not something we created

-How props and dynamic routing works https://www.youtube.com/watch?v=ESg0k2zdME4
    -we set the prop name in the router via the dynamic part of link i.e.
    path: '/custom-playlist/:playlistprop'
    -In the component, register the prop using the exact name
    props: {
        playlistprop: {
        type: String,
        },
    },
    -NOTE: EACH DYNAMIC LINK NEEDS A UNIQUE Prop Name
    -HOW I USE THIS - dynamic link prop FeaturedPlayer ---> player / playlist
    so children of the FeaturedPlayers can recieve the URL info

*/
