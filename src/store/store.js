/* eslint-disable no-unused-vars, import/prefer-default-export */
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import createMutationsSharer from 'vuex-shared-mutations';
import { playlist, portfolio } from '../grimmData';
import tags from './modules/tags';
import * as actions from './actions';
import * as getters from './getters';
import * as mutations from './mutations';


/*


*/
Vue.use(Vuex);

export const store = new Vuex.Store({

  state: {
    /* GETTERS - the state is injected into the app via getters rather than state due to the modular store and the fact
     that getters share universal name space. Be aware some state comes from the router and the vuex-router plugin, see getters file */
    featuredWork: '', // The object that holds the link to picture, mp3, and descriptions / details.
    // playlist, // Currently entire Grimm Playlist. Not used currently because of updates
    portfolio, // Currently Entire Grimm Portfolio
    featuredTags: [], // Tags that the featuredWork has
    currentPlaylist: '', // Might not be necessary...the currentPlaylist getter
    customPlaylist: [],
    collection: '',
    genre: 'none', // For the genre filter
    genrePlaylist: [],

    siteDomain: 'localhost:8080/', // Site domain used for creating dynamic links
    detailedInfoView: false,
    flashIn: false, // Flashes are used for background animations when adding tracks to Custom Playlist
    flashOut: false,

    playOrPause: false, // true did not work to fix mobile play issue
    showPlayPause: false, // for mobile test, may be removed
    autoPlay: false, // When loading a track using the loadWork mixin, if true, the player will check on the update() hook an autoplay. It is set to back to false in the update() hook after starting.
    volume: 50,
  },
  getters, // See note at the top of state
  mutations,
  actions,
  modules: {
    tags,
  },
  plugins: [createPersistedState(), createMutationsSharer({ predicate: ['addToCustomPlaylist', 'removeFromCustomPlaylist', 'clearCustomPlaylist', 'setCustomPlaylistState'] })],
});
