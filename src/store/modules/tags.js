
export default {
  state: {
    // featuredTags: [],
    selectedTags: [],
    tagState: false,
  },
  getters: {
    // featuredTags: state => state.featuredTags,
    selectedTags: state => state.selectedTags,
    tagState: state => state.tagState,
  },
  mutations: {
    selectTag: (state, tagSelected) => { // Both get's rid of and adds Selected Tags to the state
      if (state.selectedTags.includes(tagSelected)) {
        state.selectedTags = state.selectedTags.filter(tag => tag !== tagSelected);
      } else {
        state.selectedTags.push(tagSelected);
      }
    },
    clearTagSelections(state) {
      state.selectedTags = [];
    },
    updateTagState(state, payload) {
      state.tagState = payload;
    },
  },

};


// I made these freakin getters, don't need em. but i'll leave em down here
// const getters = {
// export const featuredTags = state => state.featuredTags;
// export const selectedTags = state => state.selectedTags;
// export const tagState = state => state.tagState;
// };

