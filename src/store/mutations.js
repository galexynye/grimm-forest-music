// Used Named Exports to each function import all in store.js
// in store.js ----- import * as action from './actions' ---- the * as action creates an object


import { titleToURL } from '../grimmFunctions';
import { playlist } from '../grimmData';


export const setFeaturedWork = (state, payload) => {
  state.featuredWork = payload;
  if (payload.tags) {
    state.featuredTags = payload.tags;
  } else {
    state.featuredTags = [];
  }
};

export const addToCustomPlaylist = (state, payload) => {
  state.customPlaylist.push(payload.work);
};

export const removeFromCustomPlaylist = (state, payload) => {
  state.customPlaylist.splice(payload, 1);
};

export const clearCustomPlaylist = (state) => {
  state.customPlaylist = [];
};

export const showHideDetailedInfo = (state, payload) => {
  state.detailedInfoView = payload;
};

export const updateGenre = (state, payload) => {
  state.genre = payload;
};

export const setGenrePlaylist = (state, payload) => { // Need to refactor, get the actual function out of the Genre component
  state.genrePlaylist = payload;
};

export const setVolume = (state, payload) => {
  state.volume = payload;
};


export const flashIn = (state, payload) => {
  state.flashIn = payload;
};
export const flashOut = (state, payload) => {
  state.flashOut = payload;
};

export const setPlayPause = (state, payload) => {
  if (payload) {
    state.showPlayPause = true;
  } else {
    state.showPlayPause = false;
  }
  state.playOrPause = payload;
};

export const doPlayPause = (state) => {
  state.showPlayPause = !state.showPlayPause;
  state.playOrPause = !state.playOrPause;
};
export const changeShowPlayPause = (state) => { // for mobile test, may be removed
  state.showPlayPause = !state.showPlayPause;
};

export const setAutoPlay = (state, payload) => {
  state.autoPlay = payload;
};

// Playlist belows is from raw data should be changed when doing DynamoDB // Will just be a GET request, we will create the URL here
export const setStateViaURL = (state) => {
  if (state.route.params.tracksURL) {
    if (state.route.params.playlistStateURL === 'browse') {
      for (let i = 0; i < playlist.length; i++) {
        if (titleToURL(playlist[i].title) === state.route.params.tracksURL) {
          state.featuredTags = playlist[i].tags;
          state.featuredWork = playlist[i];
        }
      }
    }

    if (state.route.params.playlistStateURL === 'custom') {
      let customPlaylistArrayPlaylist = [];
      if (state.route.params.tracksURL) {
        let urlArray = state.route.params.tracksURL.split('_');
        for (let i = 0; i < urlArray.length; i++) {
          for (let j = 0; j < playlist.length; j++) {
            if (titleToURL(playlist[j].title) === urlArray[i]) {
              customPlaylistArrayPlaylist.push(playlist[j]);
            }
          }
        }
      }
      state.customPlaylist = customPlaylistArrayPlaylist;
    }

    if (state.route.params.playlistStateURL === 'collection') {
      state.collection = state.route.params.tracksURL; // The collection in the state is only the Collection title and not the collection object
    }
  }
};


export const setCustomPlaylistState = (state, payload) => { // was used for back and forward buttons
  let customPlaylistArrayPlaylist = [];
  let urlArray = payload.split('_');
  for (let i = 0; i < urlArray.length; i++) {
    for (let j = 0; j < playlist.length; j++) {
      if (titleToURL(playlist[j].title) === urlArray[i]) {
        customPlaylistArrayPlaylist.push(playlist[j]);
      }
    }
  }
  state.customPlaylist = customPlaylistArrayPlaylist;
};

