import { titleToURL } from '../grimmFunctions';
import { playlist } from '../grimmData';

export const featuredWork = state => state.featuredWork; // URL / NOT URL - needs somefucking updating
export const featuredTags = state => state.featuredTags;

export const portfolioWork = state => state.route.params.portfolioWork;

export const playlistState = state => state.route.params.playlistStateURL; // Used For Conditional rendering of v-ifs in playlist and featuredWork
export const customPlaylist = state => state.customPlaylist; //

export const collection = state => state.collection;


export const genre = state => state.genre;
export const genrePlaylist = state => state.genrePlaylist; // this is set by a mutation in the genre component

export const siteDomain = state => state.siteDomain;
export const detailedInfoView = state => state.detailedInfoView;
export const flashIn = state => state.flashIn;
export const flashOut = state => state.flashOut;
export const playOrPause = state => state.playOrPause;
export const showPlayPause = state => state.showPlayPause; // maybe will be useless...for testing mobile


export const stateVolume = state => state.volume;
export const autoPlay = state => state.autoPlay;


export const currentPlaylist = (state, getters) => { // Uses the Getters below to created the currently rendered playlist
  let currentPlaylistReturn = [];
  if (state.route.params.playlistStateURL === 'browse') {
    currentPlaylistReturn = getters.tagFilterPlaylist;
  } else if (state.route.params.playlistStateURL === 'custom') {
    currentPlaylistReturn = getters.customPlaylist;
  } else if (state.route.params.playlistStateURL === 'collection') {
    currentPlaylistReturn = getters.collectionPlaylist;
  } else if (state.route.params.playlistStateURL === 'link') {
    currentPlaylistReturn = getters.linkPlaylist;
  }
  return currentPlaylistReturn;
};

/* In the below getters, we are currently using the playlist directly imported from grimmData above. In the future, an action will search DynamoDB
and return a playlist, then a mutation will set that playlist a property in the state that wil be referenced to below instead of playlist.
*/
export const collectionPlaylist = (state) => { // This will need to be rewritten to account for the track order
  let collectionPlaylistReturn = [];
  for (let i = 0; i < playlist.length; i++) {
    if (titleToURL(playlist[i].collection.title) === state.route.params.tracksURL) {
      collectionPlaylistReturn.push(playlist[i]); // This will need to be reordered based on the collection order
    }
  }
  let orderedArray = collectionPlaylistReturn.sort((a, b) => a.collection.trackNumber - b.collection.trackNumber);
  return orderedArray;
};

export const linkPlaylist = (state) => {
  if (state.route.params.tracksURL) {
    let linkPlaylistArrayPlaylist = [];
    let urlArray = state.route.params.tracksURL.split('_');
    for (let i = 0; i < urlArray.length; i++) {
      // for (let j = 0; j < state.playlist.length; j++) {
      for (let j = 0; j < playlist.length; j++) {
        if (titleToURL(playlist[j].title) === urlArray[i]) {
          linkPlaylistArrayPlaylist.push(playlist[j]);
        }
      }
    }
    return linkPlaylistArrayPlaylist;
  }
  return [];
};

export const tagFilterPlaylist = (state, getters) => {
  let startPlaylist = []; // Need to use this because when a Genre is selected, we can use tags on that genre
  let tagPlaylistReturn = [];
  if (state.genre !== 'none') {
    startPlaylist = state.genrePlaylist;
  } else {
    startPlaylist = playlist;
  }

  if (getters.selectedTags.length === 0) {
    return startPlaylist;
  }
  if (getters.tagState) { // Subtractive Tag filtering
    startPlaylist.forEach((track) => {
      let totalMatch = 0;

      getters.selectedTags.forEach((tag) => {
        let testArray = track.tags.map(x => x.toLowerCase());
        if (testArray.includes(tag.toLowerCase())) {
          totalMatch++;
        }
      });
      if (totalMatch === getters.selectedTags.length) {
        tagPlaylistReturn.push(track);
      }
    });
    return tagPlaylistReturn;
  }
  startPlaylist.forEach((track) => { // Additive Tag Filtering
    getters.selectedTags.forEach((tag) => {
      let testArray = track.tags.map(x => x.toLowerCase());
      if (testArray.includes(tag.toLowerCase()) && !tagPlaylistReturn.includes(track)) {
        tagPlaylistReturn.push(track);
      }
    });
  });
  return tagPlaylistReturn;
};


// export const inCustomPlaylist = (state, payload) => {
//   state.customPlaylist.includes(payload);
// };
