/*
Genres / Categories
https://www.killertracks.com/#!/en/browse.aspx
Film Score
Alternative
Folk
Drama
Blues
Comedy
Folk
Cinematic


Popular Are whats below
Blues
Childrens
Classical
Country
Dance-Electronica
Dubstep
Easy Listening
Funk
Jazz
Hip hop
Indie-Alternative Rock
Latin
Metal
Pop
Rap
Reggae
Rock
Roots
Techno
Vocal
World

*/


export const portfolio = [
  // Check if the following is on youtube video or embeds ?rel=0&amp;showinfo=0
  // Just make sure to check the correct embed procedure when getting the link
  /* */
  {
    title: 'Rift',
    image: require('../src/assets/portfolio/Rift.jpg'),
    embed: { site: 'youtube', link: 'https://www.youtube-nocookie.com/embed/H0Wj1DL-lyg?rel=0&amp;showinfo=0' },
    description: 'This is where a project description goes.',
    type: 'Original Song',
    business: false,
  },
  {
    title: 'Los Ageless',
    image: require('./assets/portfolio/LosAgeless Thumbnailsquare.jpg'),
    embed: { site: 'youtube', link: 'https://www.youtube-nocookie.com/embed/QUCcSetg-Gc' },
    description: 'If this were an advertisement it might include some of the information below.',
    type: 'Cover Song',
    business: { client: 'Dairy Queen', agency: 'Bobby Booshays', production: 'Hair Nuts' },
  },
  {
    title: 'Abilities',
    image: require('./assets/portfolio/Abilities.jpg'),
    embed: { site: 'youtube', link: 'https://www.youtube-nocookie.com/embed/_YxHlgMu_vQ?rel=0&amp;showinfo=0' },
    description: "This is where a project description goes.'",
    type: 'Original Song',
    business: false,
  },
];


export const tags = ['Adventurous', 'Abstract', 'Atmospheric',
  'Bed', 'Beauty', 'Bitter-Sweet', 'Building', 'Chill', 'Cinematic', 'Comical',
  'Competetive', 'Dark', 'Dramatic', 'Driving', 'Electronic', 'Emotional', 'Epic',
  'Fun', 'Groove', 'Happy', 'Hybrid', 'Inspirational', 'Intense',
  'Love', 'Motivational', 'Magical', 'Mysterious', 'Orchestral',
  'Party', 'Patriotic', 'Percussion', 'Quirky', 'Rage', 'Romantic',
  'Sad', 'Scary', 'Sexy', 'Song', 'Strange', 'Suspense',
  'Tension', 'Uplifting', 'Vocals'];


export const genres = ['Ambient', 'Blues', 'Electronic', 'Film Score', 'Folk', 'Indie', 'Latin', 'Pop', 'Rock', 'Trailor'];

/* If a work has no image, add link to a default image ... else the site breaks
THERE CAN BE NO TRACK TITLES WITH THE SAME NAME
Track without a collection, collection should be empty string
*/

export const playlist = [
  {
    title: 'A Good Thought',
    genre: 'Film Score',
    tags: ['Abstract', 'Atmospheric', 'Chill', 'Driving', 'Inspirational', 'Motivational', 'Hybrid', 'Quirky', 'Uplifting', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/aGoodThought/AGoodThought-50.jpg'),
    audio: require('../src/assets/music/aGoodThought/AGoodThought_v2_turnDownVox_128.mp3'),
    tempo: 94,
    key: '',
    collection: { title: 'Misc', trackNumber: 1, description: 'Miscellaneous tracks' },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'Unique and inspirational with string, electronic and sampled vocal elements.',
  },

  {
    title: 'Happy Latin',
    genre: 'Latin',
    tags: ['Comical', 'Fun', 'Groove', 'Happy', 'Party', 'Percussion', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/happyLatin/HappyLatin_500x500.jpg'),
    audio: require('../src/assets/music/happyLatin/HappyLatin_Remixed_v3_turnDownBassSlight_128.mp3'),
    tempo: 94,
    key: '',
    collection: { title: 'Misc', trackNumber: 1, description: 'Miscellaneous tracks' },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'A percussion packed happy latin track ending with a growling trumpet.',
  },
  {
    title: 'Fuzzy Chaos',
    genre: 'Rock',
    tags: ['Epic', 'Intense', 'Competetive', 'Party', 'Rage', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/fuzzyChaos/fuzzyChaos-50.jpg'),
    audio: require('../src/assets/music/fuzzyChaos/FuzzyChaos_fromV3Rockcompetition_v3_revisitRemix_128.mp3'),
    tempo: 94,
    key: '',
    collection: { title: 'Misc', trackNumber: 1, description: 'Miscellaneous tracks' },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'Heavey distorted guitar. Strange robotic vocals. Chaos.',
  },


  {
    title: 'Waiting Game',
    genre: 'Film Score',
    tags: ['Bed', 'Cinematic', 'Dark', 'Dramatic', 'Driving', 'Electronic', 'Hybrid', 'Intense', 'Mysterious', 'Suspense', 'Tension', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 6, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'A dark electronic tension bed featuring mysterious abstract female vocals and strings.',
    tempo: 77,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/Spy5_v2_WaitingGame_turnDownOnePerc_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'The Big Evil',
    genre: 'Film Score',
    tags: ['Cinematic', 'Dark', 'Dramatic', 'Epic', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Scary', 'Suspense'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 5, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'A mysterious hybrid track that builds into an epic orchetral finally personifying dark times.',
    tempo: 82,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/Spy3_v2_theBigEvil_bitlouder_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'The Drones',
    genre: 'Film Score',
    tags: ['Bed', 'Cinematic', 'Dark', 'Dramatic', 'Driving', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Scary', 'Suspense', 'Tension'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 4, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'Droning on mysteriously with electronic and orchestral elements, this tension bed keeps a suspensful mood brewing',
    tempo: 71,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/Spy4_v1_theDrone_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },

  {
    title: 'Bed Of Thorns',
    genre: 'Film Score',
    tags: ['Bed', 'Cinematic', 'Dark', 'Dramatic', 'Electronic', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Scary', 'Suspense', 'Tension', 'Thriller'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 2, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'A strange synth surrounded by strings and dark, deep percussion build a mysterious tension bed filled with suspense.',
    tempo: 59,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/BedOfThorns_v3_RevistAndRemix_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'Discovering Secrets',
    genre: 'Film Score',
    tags: ['Epic', 'Spy', 'Thriller', 'Hybrid', 'Electronic', 'Cinematic', 'Dark', 'Dramatic', 'Driving', 'Epic', 'Intense', 'Mysterious', 'Orchestral', 'Scary', 'Suspense', 'Tension'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 1, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'A driving electronic orchestral hybrid track steeped in mystery building into an intense beat.',
    tempo: 116,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/Spy2_DiscoveringSecrets_v2_SnareAdjust_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },

  {
    title: 'Spies Among Us',
    genre: 'Film ',
    tags: ['Building', 'Cinematic', 'Dark', 'Dramatic', 'Epic', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Tension'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 7, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'An growing orchestral electro piece with dark driving tension, strange string melodies, odd percussion and plenty of mystery.',
    tempo: 112,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/SpiesAmongUs_spy7v1_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'Breaking In',
    genre: 'Film Score',
    tags: ['Building', 'Cinematic', 'Dark', 'Dramatic', 'Driving', 'Electronic', 'Epic', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Scary', 'Suspense', 'Thriller'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 3, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'Starting with impactful orchestral hits this track evolves into a mysterious, thriling beat.',
    tempo: 112,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/BreakingIn_v2_RemasterAddBit_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'Bad Thoughts',
    genre: 'Film Score',
    tags: ['Bed', 'Cinematic', 'Dark', 'Dramatic', 'Hybrid', 'Intense', 'Mysterious', 'Orchestral', 'Tension'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    codes: { isrc: 'GBBGL1704460Fake' },
    collection: { title: 'Cinematic Spy', trackNumber: 8, description: 'An album of intense, mysterious tracks great for dramatic film or television.' },
    description: 'A dark tension bed with circling strings, uncomfortable synths resolving into an ominious last note.',
    tempo: 88,
    key: '',
    image: require('../src/assets/music/cinematicSpy/CinematicSpy_500-50.jpg'),
    audio: require('../src/assets/music/cinematicSpy/Spy6_v1_badThoughts_128.mp3'),
    versions: [],
    dateAdded: 20180806,
  },
  {
    title: 'Rift',
    genre: 'Rock',
    tags: ['Chill', 'Groove', 'Song', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/syntheticVolk/syntheticVolk_albumCover.jpg'),
    audio: require('../src/assets/music/syntheticVolk/Rift_mp3_128.mp3'),
    tempo: 76,
    key: 'A Minor',
    collection: { title: 'Synthetic Volk', trackNumber: 3 },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'A simple groove..."If the river don\'t drown me where she gonna spit me out?"',
  },
  {
    title: 'Karma',
    genre: 'Indie',
    tags: ['Abstract', 'Atmospheric', 'Dark', 'Song', 'Strange', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/syntheticVolk/syntheticVolk_albumCover.jpg'),
    audio: require('../src/assets/music/syntheticVolk/Karma_mp3_128.mp3'),
    tempo: 110,
    key: '',
    collection: { title: 'Synthetic Volk', trackNumber: 3 },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'Vocal harmonies repeating the phrase "Karma" in ostinato as ominous lyrics ',
  },
  {
    title: 'Abilities',
    genre: 'Folk',
    tags: ['Beauty', 'Emotional', 'Sad', 'Song', 'Vocals', 'Bitter-Sweet'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/syntheticVolk/syntheticVolk_albumCover.jpg'),
    audio: require('../src/assets/music/syntheticVolk/Abilities_mp3_128.mp3'),
    tempo: 113,
    key: 'C Major',
    collection: { title: 'Synthetic Volk', trackNumber: 3 },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'A sombre folk song with emotional vocals, acoustic guitar, harmonic, piano and bass.',
  },
];

/*

NOTE: Album and Track are the primary keys and are URL friendly
(Note: Function will be updated to remove Underscores as well)
function titleToURL(workTitle) {
  if (workTitle) {
    return workTitle.replace(/\W/g, '-').toLowerCase();
  }
  return false;
}

==========================================================
        Upload Format For a Track Via aws cli
        NOTE: ALL STRINGS NEED DOUBLE QUOTES (CHECK TAGS AND EVERY STRING)
        NOTE: ALL NUMBERS ARE PASSED AS STRINGS (use template Below)
        NOTE: Escape quotes will not work :(
==========================================================
==========================================================

STRINGIFY Version to be passed into DynamoDB

'{
    "Album":{"S": "cinematic-spy"},
    "Track":{"S": "waiting-game"},
    "title":{"S": "Waiting Game"},
    "genre":{"S": "Film Score"},
    "tags": {"SS":["Bed", "Cinematic", "Dark", "Dramatic", "Driving", "Electronic", "Hybrid", "Intense", "Mysterious", "Suspense", "Tension", "Vocals"]},
    "author":{
      "L":[
        {
          "M": {
            "name": {"S": "Alex Nye"},
            "pro": {"S": "ASCAP" }
          }
        }
      ]
    },
    "publishing":{
      "L":[{
        "M": {
          "name": {"S": "Sexy Russian Music"},
          "pro": {"S": "ASCAP" }
        }
      }]
    },
    "codes":{
      "M": {
        "isrc": {"S": "GBBGL1704460Fake"}
      }
    },
    "collection":{
      "M": {
        "title": {"S":"Cinematic Spy"},
        "trackNumber":{"N":"6"},
        "description": {"S":"An album of intense, mysterious tracks great for dramatic film or television."}
        }
      },
    "description":{"S": "A dark electronic tension bed featuring mysterious abstract female vocals and strings."},
    "tempo":{"N": "69"},
    "key":{"S": "MISSING"},
    "image":{"S": "MISSING"},
    "audio":{"S": "MISSING"},
    "versions":{"SS":["MISSING"]}
  }'


==================================================
==================================================


==================================================
Submission Zone (create json below)
==================================================
 {
    title: 'Rift',
    genre: 'Rock',
    tags: ['Chill', 'Groove', 'Song', 'Vocals'],
    author: [{ name: 'Alex Nye', pro: 'ASCAP' }],
    publishing: [{ name: 'Sexy Russian Music', pro: 'ASCAP' }],
    image: require('../src/assets/music/syntheticVolk/syntheticVolk_albumCover.jpg'),
    audio: require('../src/assets/music/syntheticVolk/Rift_mp3_128.mp3'),
    tempo: 76,
    key: 'A Minor',
    collection: { title: 'Synthetic Volk', trackNumber: 3 },
    dateAdded: 20180630,
    codes: { isrc: 'GBBGL1704460Fake' },
    description: 'A simple groove..."If the river don\'t drown me where she gonna spit me out?"',
  },

'{
    "Album":{"S": "synthetic-volk"},
    "Track":{"S": "rift"},
    "title":{"S": "Rift"},
    "genre":{"S": "Rock"},
    "tags": {"SS":["Chill", "Groove", "Song", "Vocals"]},
    "author":{
      "L":[
        {
          "M": {
            "name": {"S": "Alex Nye"},
            "pro": {"S": "ASCAP" }
          }
        }
      ]
    },
    "publishing":{
      "L":[{
        "M": {
          "name": {"S": "Sexy Russian Music"},
          "pro": {"S": "ASCAP" }
        }
      }]
    },
    "codes":{
      "M": {
        "isrc": {"S": "GBBGL1704460Fake"}
      }
    },
    "collection":{
      "M": {
        "title": {"S":"Synthetic Volk"},
        "trackNumber":{"N":"3"},
        "description": {"S":"The Sexy Russians ecletic second EP."}
        }
      },
    "description":{"S": "A simple groove..blahblah"},
    "tempo":{"N": "76"},
    "key":{"S": "A Minor"},
    "image":{"S": "MISSING"},
    "audio":{"S": "MISSING"},
    "versions":{"SS":["MISSING"]}
  }'


==================================================
AWS INFO / Commands
==================================================
https://docs.aws.amazon.com/cli/latest/reference/dynamodb/put-item.html
https://docs.aws.amazon.com/cli/latest/reference/dynamodb/update-item.html
https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/LegacyConditionalParameters.AttributeUpdates.html

CLI Put-item Example (Deletes and Replaces item if it exists)
aws dynamodb put-item --table-name activeMusicCatalog --item '{"Album":{"S": "Misc"},"Track":{"S": "fuzzy-chaos"}}'

CLI Update-item Example

https://gy0b4kocja.execute-api.us-east-1.amazonaws.com/dev/music API Link

*/

